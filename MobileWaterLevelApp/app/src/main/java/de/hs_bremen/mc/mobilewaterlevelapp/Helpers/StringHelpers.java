package de.hs_bremen.mc.mobilewaterlevelapp.Helpers;

import java.util.Locale;

public class StringHelpers {
    public static String getLongestString(String s1, String s2) {

        s1 = s1.toUpperCase(new Locale("de", "DE"));
        s2 = s2.toUpperCase(new Locale("de", "DE"));

        if (s1.equals(s2)) {
            return s1;
        } else if (s1.length() > s2.length()) {
            return s1;
        } else if (s1.length() < s2.length()) {
            return s2;
        } else {
            return s2;
        }
    }
}
