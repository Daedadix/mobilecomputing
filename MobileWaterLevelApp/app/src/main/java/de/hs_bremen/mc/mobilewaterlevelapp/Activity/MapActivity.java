package de.hs_bremen.mc.mobilewaterlevelapp.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.MapTileProviderBasic;
import org.osmdroid.tileprovider.tilesource.ITileSource;
import org.osmdroid.tileprovider.tilesource.XYTileSource;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.Polygon;
import org.osmdroid.views.overlay.TilesOverlay;
import org.osmdroid.views.overlay.compass.CompassOverlay;
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import de.hs_bremen.mc.mobilewaterlevelapp.Objects.Station;
import de.hs_bremen.mc.mobilewaterlevelapp.R;

public class MapActivity extends Activity implements ItemizedIconOverlay.OnItemGestureListener {

    public MapView map = null;
    public CompassOverlay compassOverlay = null;

    private double longitude = 0.0;
    private double latitude = 0.0;
    private int radius = 0;
    private ArrayList<Station> stationList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /*
         * Parts of this code can be found in the osmdroid documentaion
         * @link https://github.com/osmdroid/osmdroid/wiki
         */

        super.onCreate(savedInstanceState);

        //Predefined values
        Bundle extras = getIntent().getExtras();

        //check if extras are delivered
        if (extras != null) {
            this.latitude = extras.getDouble("latitude");
            this.longitude = extras.getDouble("longitude");
            if (extras.getInt("radius") != 0) {
                this.radius = extras.getInt("radius");
            }
            if (getIntent().getSerializableExtra("stations") != null) {
                Log.d("XXX", "Station List is there!");
                this.stationList = (ArrayList<Station>)
                        getIntent().getSerializableExtra("stations");
                Log.d("XXX", "Station List in Map Activity has " + stationList.size() + " items");
            } else {
                Log.d("XXX", "No extra stations delivered");
            }
        } else {
            throw new NoSuchElementException();
        }

        //Permissions
        Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        setContentView(R.layout.activity_map);

        //Declare map object
        map = findViewById(R.id.map);

        //Set first tile source layer
        map.setTileSource(new XYTileSource("MAPNIK", 1, 28, 256, ".png",
                new String[]{
                        "https://a.tile.openstreetmap.org/",
                        "https://b.tile.openstreetmap.org/",
                        "https://c.tile.openstreetmap.org/"}, "© OpenStreetMap contributors"));

        //Create tile source overlay
        final ITileSource seaMapTileSource = new XYTileSource("OpenSeaMap",
                1, 18, 256, ".png", new String[]{"https://tiles.openseamap.org/seamark/"}, "OpenSeaMap");
        final MapTileProviderBasic tileProvider = new MapTileProviderBasic(ctx, seaMapTileSource);
        final TilesOverlay seaMapOverlay = new TilesOverlay(tileProvider, ctx);
        seaMapOverlay.setLoadingBackgroundColor(Color.TRANSPARENT);

        //Set tile source overlay
        map.getOverlays().add(seaMapOverlay);

        //Map options
        map.setTilesScaledToDpi(true);
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);

        //Set starting position
        IMapController mapController = map.getController();
        mapController.setZoom(15.0);
        GeoPoint startPoint = new GeoPoint(latitude, longitude);
        mapController.setCenter(startPoint);

        //Set starting position marker
        Marker startMarker = new Marker(map);
        startMarker.setPosition(startPoint);
        startMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        startMarker.setIcon(getDrawable(R.drawable.ic_place_black_24dp));
        map.getOverlays().add(startMarker);

        //set nearby station if available
        if (stationList != null) {
            //set radius

            Polygon oPolygon = new Polygon(map);
            final double radius = this.radius;
            ArrayList<GeoPoint> circlePoints = new ArrayList<>();
            for (float f = 0; f < 360; f += 1){
                circlePoints.add(new GeoPoint(this.latitude, this.longitude).destinationPoint(radius * 10, f));
            }
            oPolygon.setPoints(circlePoints);
            map.getOverlays().add(oPolygon);

            //append other stations to list
            ArrayList<OverlayItem> items = new ArrayList<>();
            for (Station station : stationList) {
                OverlayItem item = new OverlayItem(station.getName(), station.getUUID(), new GeoPoint(station.getLatitude(), station.getLongitude()));
                item.setMarker(getDrawable(R.drawable.ic_place_black_24dp));
                items.add(item);
            }

            ItemizedOverlayWithFocus mOverlay = new ItemizedOverlayWithFocus<>(items, this, this);
            mOverlay.setFocusItemsOnTap(true);

            map.getOverlays().add(mOverlay);
        }

        //Set compass overlay
        this.compassOverlay = new CompassOverlay(ctx, new InternalCompassOrientationProvider(ctx), map);
        this.compassOverlay.enableCompass();
        map.getOverlays().add(this.compassOverlay);

        //Refresh map
        map.invalidate();
    }

    /**
     * @method onResume
     */
    @Override
    public void onResume() {
        super.onResume();
        Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
        map.onResume();
    }

    /**
     * @method onPause
     */
    @Override
    public void onPause() {
        super.onPause();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Configuration.getInstance().save(this, prefs);
        map.onPause();
    }

    @Override
    public boolean onItemSingleTapUp(int index, Object item) {
        return false;
    }

    @Override
    public boolean onItemLongPress(int index, Object item) {
        return false;
    }
}
