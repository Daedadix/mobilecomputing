package de.hs_bremen.mc.mobilewaterlevelapp.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import de.hs_bremen.mc.mobilewaterlevelapp.Objects.Favs;

@Database(entities = {Favs.class}, version = 1)
public abstract class FavsDatabase extends RoomDatabase {
    public abstract FavsDao favsDao();

    private static FavsDatabase fInstance;

    public static synchronized FavsDatabase getInstance(Context context) {
        if (fInstance == null) {

            //Wenn noch keine Instanz der Datenbank exisitert wird hier eine erstellt. Hier verwenden wir auch das in der Präsentation angesprochene .allowMainThreadQueries()
            //Das diese Methode funktioniert, zeigt das FavFragment. In der DetailActivity nutzen wir die Lösung eines neuen Threads.
            fInstance = Room
                    .databaseBuilder(context.getApplicationContext(), FavsDatabase.class, "FavsDatabase")
                    .allowMainThreadQueries()
                    .build();
        }
        return fInstance;
    }
}
