package de.hs_bremen.mc.mobilewaterlevelapp.Objects;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;

import static de.hs_bremen.mc.mobilewaterlevelapp.Objects.Enums.APICallType;

public class APIData {

    private URL url;

    public APIData() {

    }

    public APIData(APICallType type) {
        this.url = this.setURL(type);
    }

    public APIData(APICallType type, String searchTerm) {
        this.url = this.setURL(type, searchTerm);
    }

    public APIData(APICallType type, LocationData locationData, int radius) {
        this.url = this.setURL(type, locationData, radius);
    }

    private URL setURL(APICallType type) {
        switch (type) {
            case AllWaters:
                try {
                    return new URL("https://www.pegelonline.wsv.de/webservices/rest-api/v2/waters.json");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            default:
                return null;
        }
    }

    private URL setURL(APICallType type, String searchTerm) {
        switch (type) {
            case StationsByWater:
                try {
                    return new URL("https://www.pegelonline.wsv.de/webservices/rest-api/v2/" + MessageFormat.format("stations.json?waters={0}", searchTerm));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            case DataByStation:
                try {
                    return new URL("https://www.pegelonline.wsv.de/webservices/rest-api/v2/" + MessageFormat.format("stations/{0}/W/measurements.json?start=P7D", searchTerm));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            case CurrentMeasurement:
                try {
                    return new URL("  https://www.pegelonline.wsv.de/webservices/rest-api/v2/" + MessageFormat.format("stations/{0}/W/currentmeasurement.json", searchTerm));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

            default:
                return null;
        }
    }

    private URL setURL(APICallType type, LocationData locationData, int radius) {
        switch (type) {
            case StationsByLocation:
                try {
                    String radiusValue = "" + radius + "";
                    if (radiusValue.contains(".")) {
                        radiusValue = radiusValue.replace(".", "");
                    }

                    return new URL("https",
                            "pegelonline.wsv.de/webservices/rest-api/v2",
                            MessageFormat.format(
                                    "stations.json?latitude={0}&longitude={1}&radius={2}",
                                    locationData.getLatitude(),
                                    locationData.getLongitude(),
                                    radiusValue
                            )
                    );
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            default:
                return null;
        }
    }


    public String getRawDataFromURL() throws IOException {
        HttpURLConnection connection = null;
        BufferedReader reader = null;

        try {
            connection = (HttpURLConnection) this.url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }

        InputStream stream = null;

        try {
            assert connection != null;
            stream = connection.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (stream != null) {
            reader = new BufferedReader(new InputStreamReader(stream));
        }

        StringBuilder buffer = new StringBuilder();
        String line;

        if (reader != null) {
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }
        }

        return buffer.toString();
    }

    public URL getURL() {
        return this.url;
    }
}
