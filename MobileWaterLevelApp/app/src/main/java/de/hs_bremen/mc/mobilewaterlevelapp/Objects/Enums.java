package de.hs_bremen.mc.mobilewaterlevelapp.Objects;

public class Enums {
    public enum ActivitySelect {Main, Map, Settings}

    public enum APICallType {AllWaters, StationsByWater, StationsByLocation, DataByStation, CurrentMeasurement}
}

