package de.hs_bremen.mc.mobilewaterlevelapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import de.hs_bremen.mc.mobilewaterlevelapp.Database.FavsDao;
import de.hs_bremen.mc.mobilewaterlevelapp.Database.FavsDatabase;
import de.hs_bremen.mc.mobilewaterlevelapp.Objects.APIData;
import de.hs_bremen.mc.mobilewaterlevelapp.Objects.Favs;
import de.hs_bremen.mc.mobilewaterlevelapp.R;

import static de.hs_bremen.mc.mobilewaterlevelapp.Objects.Enums.APICallType.CurrentMeasurement;
import static de.hs_bremen.mc.mobilewaterlevelapp.Objects.Enums.APICallType.DataByStation;

public class DetailActivity extends AppCompatActivity {

    // Platzhalterdaten für den Fall das Informationen nicht übergeben worden.
    private double longitude = 0.0;
    private double latitude = 0.0;
    private String name = "Fehler";
    private String uuid = "000";

    JSONArray json = null;
    JSONObject jsonObj = null;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Beispieldaten mit denen vom SearchFragment übergebenen Informationen überschreiben
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.latitude = extras.getDouble("latitude");
            this.longitude = extras.getDouble("longitude");
            this.name = extras.getString("name");
            this.uuid = extras.getString("uuid");
        } else {
            throw new NoSuchElementException();
        }

        // Einbinden der Datenbank
        FavsDatabase db = FavsDatabase.getInstance(getApplicationContext());
        FavsDao favsDao = db.favsDao();

        // Setzt den Namen der Station als Titel in die Toolbar
        setTitle(this.name);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try{
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e){
            e.printStackTrace();
        }

        Button mapButton = findViewById(R.id.mapButton);

        TextView textViewTimestamp = findViewById(R.id.textViewTimestamp);
        TextView textViewCurrent = findViewById(R.id.textViewCurrent);
        TextView textViewTrend = findViewById(R.id.textViewTrend);
        ImageView imageViewTrend = findViewById(R.id.imageViewTrend);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        APIData current = new APIData(CurrentMeasurement, this.uuid);
        try {
            jsonObj = new JSONObject(current.getRawDataFromURL());
            Double CurrentMeasurementValue = jsonObj.getDouble("value");
            textViewCurrent.setText(MessageFormat.format("{0} cm", CurrentMeasurementValue));
            textViewTimestamp.setText(MessageFormat.format(" {0}", jsonObj.getString("timestamp")));

            // grafisches Anzeigen der Tendenz, die Pfeile zeigen jeweils an ob der Wasserstand fällt, gleichbleibt oder steigt.
            switch (jsonObj.getInt("trend")){
                case -1:
                    textViewTrend.setText(R.string.tendencyFalling);
                    imageViewTrend.setImageResource(R.drawable.ic_arrow_downward_black_24dp);
                    break;

                case 0:
                    textViewTrend.setText(R.string.tendencyConsistent);
                    imageViewTrend.setImageResource(R.drawable.ic_arrow_forward_black_24dp);
                    break;

                case 1:
                    textViewTrend.setText(R.string.tendencyRising);
                    imageViewTrend.setImageResource(R.drawable.ic_arrow_upward_black_24dp);
                    break;
            }
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }

        // Übergeben der Längen- und breitengerade, sowie Aufruf der OpenSeaMap zur Anzeige der Stationsposition beim klicken des dafür erstellten Buttons.
        mapButton.setOnClickListener(view -> {
            Intent intent = new Intent(DetailActivity.this, MapActivity.class);

            intent.putExtra("latitude", latitude);
            intent.putExtra("longitude", longitude);
            DetailActivity.this.startActivity(intent);
        });

        // Erzeugen und füllen des Liniendiagramms zur Anzeige des Wasserstands der letzten 7 Tage (MPAndroidChart)
        LineChart chart = findViewById(R.id.chart);
        List<Entry> entries = new ArrayList<>();

        try {
            APIData pegel = new APIData(DataByStation, this.uuid);
            json = new JSONArray(pegel.getRawDataFromURL());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Es existieren Stationen die keine Zeitreihe für die letzten sieben Tage besitzen.
        // Ein Grund dafür kann sein, das die Station seit längerer Zeit nicht mehr aktiv misst, aber immer noch ein Teil des Systems ist.
        if(json.length() > 2){
            int x = 1;
            for (int i = 0; i < json.length(); i++) {
                // Um das Laden der DetailView zu beschleunigen, wird nur jeder zweite Wert zum Diagramm hinzugefügt. Dies ist besonders für Stationen die von Ebbe und Flut betroffen sind gedacht.
                // Diese Stationen schreiben Werte in kürzeren Abständen, was teilweise dazu führen kann bis zu tausend Einträge für die dargestellte Zeit existieren.
                if (i % 2 == 0) {
                    JSONObject j = new JSONObject();
                    try {
                        j = json.getJSONObject(i);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        if (j != null) {
                            entries.add(new Entry(x,(float)j.getDouble("value")));
                            x++;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            LineDataSet dataSet = new LineDataSet(entries, getString(R.string.waterLevel));

            LineData lineData = new LineData(dataSet);
            chart.setData(lineData);
            chart.getDescription().setText(getString(R.string.waterLevelLast7Days));
            chart.invalidate();
        }

        CheckBox favCheckBox = ( CheckBox ) findViewById( R.id.favCheckBox );

        // Wenn die Station bereits ein Favorit ist, wird der Text und die Checkbox selbst dementsprechend geändert.
        if(favsDao.getByUuid(uuid) > 0){
            favCheckBox.setChecked(!favCheckBox.isChecked());
            favCheckBox.setText("Favorit");
        }

        // Die CheckBox fügt eine Station zur Datenbank hinzu, oder löscht Sie wieder. Je nachdem in welchem Zustand die Checkbox ist.
        favCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                   new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Favs fa = new Favs();

                            fa.setLatitude(latitude);
                            fa.setLongitude(longitude);
                            fa.setName(name);
                            fa.setUuid(uuid);
                            try {
                                favsDao.insert(fa);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }) .start();

                    favCheckBox.setText("Favorit");
                }
                else {

                    try{
                        favsDao.deleteByUuid(uuid);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    favCheckBox.setText("kein Favorit");
                }
            }
        });
    }
}