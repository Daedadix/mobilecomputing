package de.hs_bremen.mc.mobilewaterlevelapp.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Objects;

import de.hs_bremen.mc.mobilewaterlevelapp.Activity.DetailActivity;
import de.hs_bremen.mc.mobilewaterlevelapp.Helpers.JSONHelpers;
import de.hs_bremen.mc.mobilewaterlevelapp.Helpers.StringHelpers;
import de.hs_bremen.mc.mobilewaterlevelapp.Objects.APIData;
import de.hs_bremen.mc.mobilewaterlevelapp.Objects.Station;
import de.hs_bremen.mc.mobilewaterlevelapp.R;

import static android.content.Context.CONNECTIVITY_SERVICE;
import static android.content.Context.INPUT_METHOD_SERVICE;
import static android.net.ConnectivityManager.TYPE_WIFI;
import static de.hs_bremen.mc.mobilewaterlevelapp.Objects.Enums.APICallType.AllWaters;
import static de.hs_bremen.mc.mobilewaterlevelapp.Objects.Enums.APICallType.StationsByWater;


public class SearchFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public SearchFragment() {
        // Required empty public constructor
    }

    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    JSONArray json = null;
    LinkedList<Station> stationList = new LinkedList<>();
    LinkedList<String> infoList = new LinkedList<>();
    ArrayAdapter<String> adapter = null;
    ArrayAdapter<String> stationArrayAdapter = null;
    private AutoCompleteTextView riverTextView = null;
    private ListView stationListView = null;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        adapter = new ArrayAdapter<>(view.getContext(),
                android.R.layout.simple_dropdown_item_1line, GetRiverList());
        stationArrayAdapter = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_expandable_list_item_1, infoList);
        riverTextView = Objects.requireNonNull(getView()).findViewById(R.id.riverInputField);
        riverTextView.setAdapter(adapter);

        Button loadDataButton = getView().findViewById(R.id.loadDataButton);
        stationListView = getView().findViewById(R.id.stationListView);

        loadDataButton.setOnClickListener(view12 -> {
            try {
                InputMethodManager imm = (InputMethodManager) Objects.requireNonNull(getActivity()).getSystemService(INPUT_METHOD_SERVICE);
                assert imm != null;
                imm.hideSoftInputFromWindow(Objects.requireNonNull(getActivity().getCurrentFocus()).getWindowToken(), 0);
            } catch (Exception e) {
                e.printStackTrace();
            }

            stationList.clear();
            infoList.clear();

            String inputText = riverTextView.getText().toString().toUpperCase();
            APIData data = new APIData(StationsByWater, inputText);

            try {
                json = new JSONArray(data.getRawDataFromURL());
            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < json.length(); i++) {
                JSONObject j = new JSONObject();
                try {
                    j = json.getJSONObject(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    if (j != null) {
                        stationList.add(new Station(
                                j.getString("uuid"),
                                j.getString("longname"),
                                j.getInt("number"),
                                j.getDouble("km"),
                                j.getDouble("longitude"),
                                j.getDouble("latitude")
                        ));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

//            Collections.sort(stationList);

            for (Station station : stationList) {
                infoList.add(station.getName());
            }

            stationListView.setAdapter(stationArrayAdapter);
        });

        stationListView.setOnItemClickListener((adapterView, view1, i, l) -> {
            Intent intent = new Intent(getActivity(), DetailActivity.class);
            stationList.size();
            Station station = stationList.get(i);

            intent.putExtra("latitude", station.getLatitude());
            intent.putExtra("longitude", station.getLongitude());
            intent.putExtra("name", station.getName());
            intent.putExtra("uuid", station.getUUID());
            startActivity(intent);
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle("Suchen");

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public String[] GetRiverList() {
        ConnectivityManager connManager = (ConnectivityManager) Objects.requireNonNull(getActivity()).getSystemService(CONNECTIVITY_SERVICE);
        assert connManager != null;
        NetworkInfo activeNetwork = connManager.getActiveNetworkInfo();

        LinkedList<String> watersList = new LinkedList<>();

        if (activeNetwork.getType() == TYPE_WIFI) {
            //if wireless connection is enabled, get water list from the API
            JSONArray jsonWatersArray = null;
            try {
                try {
                    jsonWatersArray = new JSONArray(JSONHelpers.readJSONFile(getActivity().getAssets().open("waters.json")).toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            assert jsonWatersArray != null;
            for (int i = 0; i < jsonWatersArray.length(); i++) {
                try {
                    watersList.add((jsonWatersArray.get(i).toString()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return watersList.toArray(new String[watersList.size()]);
        } else {
            //if wireless is not enabled, load predefined file
            APIData api = new APIData(AllWaters);
            try {
                JSONArray data = new JSONArray(api.getRawDataFromURL());

                for (int i = 0; i < data.length(); i++) {
                    JSONObject dataObject = null;
                    try {
                        dataObject = data.getJSONObject(i);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    assert dataObject != null;

                    watersList.add(StringHelpers.getLongestString(dataObject.getString("longname"), dataObject.getString("shortname")));
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return watersList.toArray(new String[watersList.size()]);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
