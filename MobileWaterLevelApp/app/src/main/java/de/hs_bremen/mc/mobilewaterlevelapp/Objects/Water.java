package de.hs_bremen.mc.mobilewaterlevelapp.Objects;

import java.util.Date;
import java.util.LinkedList;

public class Water {

    private String _name;
    private LinkedList<Station> _stationList;
    private Date _updateDate;

    public Water(String name) {
        this._name = name;
        this._stationList = fetchStations(name);
        this._updateDate = new Date();
    }

    private LinkedList<Station> fetchStations(String name) {
        LinkedList<Station> list = new LinkedList<>();
        list.push(new Station("", "", 0, 0, 0.00, 0.00));
        return list;
    }

    public String getName() {
        return this._name;
    }

    public LinkedList<Station> getStationList() {
        return this._stationList;
    }

    public Date getUpdateDate() {
        return this._updateDate;
    }
}
