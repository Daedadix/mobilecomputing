package de.hs_bremen.mc.mobilewaterlevelapp.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.LinkedList;
import java.util.List;

import de.hs_bremen.mc.mobilewaterlevelapp.Activity.DetailActivity;
import de.hs_bremen.mc.mobilewaterlevelapp.Database.FavsDao;
import de.hs_bremen.mc.mobilewaterlevelapp.Database.FavsDatabase;
import de.hs_bremen.mc.mobilewaterlevelapp.Objects.Favs;
import de.hs_bremen.mc.mobilewaterlevelapp.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FavFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FavFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FavFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private ListView stationFavListView = null;
    private ArrayAdapter<String> listAdapter ;
    LinkedList<Favs> stationFavList = new LinkedList<>();


    public FavFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FavFragment.
     */
    public static FavFragment newInstance() {
        FavFragment fragment = new FavFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        stationFavListView = getView().findViewById(R.id.stationFavListView);
        listAdapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.fragment_fav_row);

        FavsDatabase db = FavsDatabase.getInstance(getActivity().getApplicationContext());
        FavsDao favsDao = db.favsDao();

        List<Favs> fa = favsDao.getAll();


        for(Favs fav:fa){
            listAdapter.add(fav.getName());
        }

        stationFavListView.setAdapter( listAdapter );

        stationFavListView.setOnItemClickListener((adapterView, view1, i, l) -> {
            Intent intent = new Intent(getActivity(), DetailActivity.class);
            fa.size();
            Favs fav = fa.get(i);

            intent.putExtra("latitude", fav.getLatitude());
            intent.putExtra("longitude", fav.getLongitude());
            intent.putExtra("name", fav.getName());
            intent.putExtra("uuid", fav.getUuid());
            startActivity(intent);
        });
    }

//    @Override
//        public void onResume() {
//            super.onResume();
//
//            FavsDatabase db = FavsDatabase.getInstance(getActivity().getApplicationContext());
//            FavsDao favsDao = db.favsDao();
//
//            List<Favs> fa = favsDao.getAll();
//
//            for(Favs fav:fa){
//                listAdapter.add(fav.getName());
//            }
//
//            stationFavListView.setAdapter( listAdapter );
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle("Favoriten");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fav, container, false);
    }
    
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
