package de.hs_bremen.mc.mobilewaterlevelapp.Objects;

import java.io.Serializable;

public class Station implements Serializable {

    private String _uuid;
    private String _name;
    private int _number;
    private double _kmPoint;
    private double _longitude;
    private double _latitude;

    public Station(String uuid, String name, int number, double kmPoint, double longitude, double latitude) {
        this._uuid = uuid;
        this._name = name;
        this._number = number;
        this._kmPoint = kmPoint;
        this._longitude = longitude;
        this._latitude = latitude;
    }

    public String getUUID() {
        return this._uuid;
    }

    public String getName() {
        return this._name;
    }

    public int getNumber() {
        return this._number;
    }

    public double getKmPoint() {
        return this._kmPoint;
    }

    public double getLongitude() {
        return this._longitude;
    }

    public double getLatitude() {
        return this._latitude;
    }

    public String getInfo() {
        return getName() + "@" + getKmPoint() + "(" + getLongitude() + " / " + getLatitude() + ")";
    }

    public void setUuid(String _uuid) {
        this._uuid = _uuid;
    }

    public void setName(String _name) {
        this._name = _name;
    }

    public void setLongitude(double _longitude) {
        this._longitude = _longitude;
    }

    public void setLatitude(double _latitude) {
        this._latitude = _latitude;
    }
}
