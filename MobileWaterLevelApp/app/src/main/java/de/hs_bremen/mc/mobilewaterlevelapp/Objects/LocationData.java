package de.hs_bremen.mc.mobilewaterlevelapp.Objects;

public class LocationData {

    private double Latitude;
    private double Longitude;

    public LocationData(double latitude, double longitude) {
        this.Latitude = latitude;
        this.Longitude = longitude;
    }


    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        Longitude = latitude;
    }
}
