package de.hs_bremen.mc.mobilewaterlevelapp.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import de.hs_bremen.mc.mobilewaterlevelapp.Objects.Favs;

@Dao
public interface FavsDao {
    // Dieser Query wird in dem FavFragment dazu verwendet, um alle Favoriten in einer Liste anzuzeigen
    @Query("SELECT * FROM favs")
    List<Favs> getAll();

    // Dieser Query wird in der DetailActivity verwendet, um Favoriten aus der Datenbank zu löschen.
    @Query("DELETE FROM favs WHERE favs.uuid = :uuid")
    void deleteByUuid(String uuid);

    // Dieser Query wird in der DetailActivity verwendet, um zu prüfen ob die Station bereits ein Favorit ist.
    @Query("SELECT * FROM favs WHERE favs.uuid = :uuid")
    int getByUuid(String uuid);

    // Dieser Query wird in der DetailActivity verwendet, um eine Station zu den Favoriten hinzuzufügen.
    @Insert
    void insert(Favs favs);
}
