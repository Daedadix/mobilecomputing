package de.hs_bremen.mc.mobilewaterlevelapp.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.LinkedList;

import de.hs_bremen.mc.mobilewaterlevelapp.Fragments.FavFragment;
import de.hs_bremen.mc.mobilewaterlevelapp.Fragments.ImprintFragment;
import de.hs_bremen.mc.mobilewaterlevelapp.Fragments.PrivacyFragment;
import de.hs_bremen.mc.mobilewaterlevelapp.Fragments.SearchFragment;
import de.hs_bremen.mc.mobilewaterlevelapp.Fragments.SettingsFragment;
import de.hs_bremen.mc.mobilewaterlevelapp.Objects.APIData;
import de.hs_bremen.mc.mobilewaterlevelapp.Objects.Enums;
import de.hs_bremen.mc.mobilewaterlevelapp.Objects.LocationData;
import de.hs_bremen.mc.mobilewaterlevelapp.Objects.Station;
import de.hs_bremen.mc.mobilewaterlevelapp.R;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, LocationListener, ImprintFragment.OnFragmentInteractionListener, SearchFragment.OnFragmentInteractionListener,
        FavFragment.OnFragmentInteractionListener, SettingsFragment.OnFragmentInteractionListener, PrivacyFragment.OnFragmentInteractionListener {

    static public final int REQUEST_LOCATION = 1;
    public double locationLatitude = 0.0d;
    public double locationLongitude = 0.0d;
    LocationManager locationManager;
    String locationProvider;

    final static int RADIUS = 50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                //TODO: Why do we need a permission
            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        33);

                //TODO: Has getting permission
            }
        } else {
            Toast.makeText(MainActivity.this, "Permission already granted", Toast.LENGTH_SHORT).show();
        }

        /*
         * To avoid exception
         * @link https://stackoverflow.com/questions/6343166/how-do-i-fix-android-os-networkonmainthreadexception/9289190#9289190
         */
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        if (savedInstanceState == null) {
            Fragment fragment = null;
            Class fragmentClass = null;
            fragmentClass = SearchFragment.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }

            FragmentManager fragmentManager = getSupportFragmentManager();
            assert fragment != null;
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        }

        try {
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        } catch (Exception ex) {
            //do something useful here
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        Class fragmentClass = null;
        switch (id) {
            case R.id.nav_main:
                fragmentClass = SearchFragment.class;
                break;
            case R.id.nav_favs:
                fragmentClass = FavFragment.class;
                break;
            case R.id.nav_map:
                fragmentClass = SearchFragment.class;
                Intent mapIntent = new Intent(this, MapActivity.class);
                mapIntent.putExtra("latitude", locationLatitude);
                mapIntent.putExtra("longitude", locationLongitude);
                mapIntent.putExtra("stations", getNearbyStations());
                mapIntent.putExtra("radius", RADIUS);
                startActivity(mapIntent);
                break;
            case R.id.nav_settings:
                fragmentClass = SettingsFragment.class;
                break;
            case R.id.nav_datenschutz:
                fragmentClass = PrivacyFragment.class;
                break;
            case R.id.nav_impressum:
                fragmentClass = ImprintFragment.class;
                break;
        }
        try {
            assert fragmentClass != null;
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        assert fragment != null;
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public LinkedList<Station> getNearbyStations() {
        LinkedList<Station> stationList = new LinkedList<>();
        APIData stationsInArea = new APIData(Enums.APICallType.StationsByLocation, new LocationData(locationLatitude, locationLongitude), RADIUS);
        try {
            JSONArray json = new JSONArray(stationsInArea.getRawDataFromURL());

            Log.d("XXX", stationsInArea.getURL().toString());
            Log.d("XXX", "Raw data from URL: " + stationsInArea.getRawDataFromURL());

            for (int i = 0; i < json.length(); i++) {
                JSONObject j = new JSONObject();
                try {
                    j = json.getJSONObject(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    if (j != null) {
                        stationList.add(new Station(
                                j.getString("uuid"),
                                j.getString("longname"),
                                j.getInt("number"),
                                j.getDouble("km"),
                                j.getDouble("longitude"),
                                j.getDouble("latitude")
                        ));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }

        Log.d("XXX", "Station List length: " + stationList.size());

        return stationList;
    }

    @Override
    public void onLocationChanged(Location location) {
        locationLatitude = location.getLatitude();
        locationLongitude = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}